// <reference path="typeScriptDefenitions/libs.d.ts" />
/// <amd-dependency path="angular-route"/>

import angular = require('angular')
import RoomsController = require('rooms/RoomsController')
import ChatController = require('chat/ChatController')
import RoomsService = require('rooms/RoomsService')

var ngModule: ng.IModule = angular.module('amdatu.chat.app', ['ngRoute']).config(['$routeProvider',
    ($routeProvider:ng.route.IRouteProvider) => {
        $routeProvider.when('/rooms', {
            controller: 'RoomsController',
            controllerAs: 'rooms',
            templateUrl: 'rooms/views/rooms.html'
        }).when('/rooms/:room', {
            controller: 'ChatController',
            controllerAs: 'chat',
            templateUrl: 'chat/views/chat.html'
        }).otherwise({redirectTo: '/rooms'});
    }
]);



ngModule.controller('RoomsController', RoomsController);
ngModule.controller('ChatController', ChatController);
ngModule.service('RoomsService', RoomsService);
ngModule.constant('BASE_URL', 'http://localhost:8080');

export = ngModule;

