// <reference path="typeScriptDefenitions/libs.d.ts" />
import Rx = require('Rx')
import SockJS = require('SockJS')
import Stomp = require('Stomp')

class RoomsService {
    static $inject = ['$http', '$q', 'BASE_URL', '$rootScope'];

    private client;
    private isConnected : boolean = false;

    constructor(private $http:ng.IHttpService, private $q:ng.IQService, private BASE_URL, private $rootScope : ng.IRootScopeService) {
    }

    private connect() : ng.IPromise<boolean> {
        var p = this.$q.defer();

        if(this.isConnected) {
            p.resolve(true);
        } else {
            var ws = new SockJS("http://localhost:15674/stomp");

            this.client = Stomp.over(ws);
            this.client.heartbeat.incoming = 0;

            var on_connect = () => {
                this.isConnected = true;
                p.resolve(true);
            };

            var on_error = (error:StompError) => {
                console.log(error);
                p.reject();
            };

            this.client.connect('guest', 'guest', on_connect, on_error, '/');
        }

        return p.promise;
    }

    getRooms() : Rx.Observable<Room[]> {
        return Rx.Observable.create((observer : Rx.Observer<Room[]>) => {
            this.$http.get(this.BASE_URL + "/rooms").success((resp) => {
                observer.onNext(resp);
                observer.onCompleted();
            });
        });
    }

    enterRoom(roomName : string) : Rx.Observable<Message> {

        console.log(roomName);

        var observable = Rx.Observable.create((observer : Rx.Observer<Message>) => {
            this.connect().then(() => {
                this.client.subscribe("/topic/" + roomName, (message) => {
                    observer.onNext(JSON.parse(message.body));
                    this.$rootScope.$digest();
                });
            });

        });


        return observable;
    }

    sendToRoom(roomName : string, message : Message) {
        this.connect().then(() => {
            this.client.send("/topic/" + roomName, {}, JSON.stringify(message));
        });
    }
}

export = RoomsService;