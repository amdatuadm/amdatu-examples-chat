interface Message {
    body : string
    from : string;
    imageUrl? : string;
}