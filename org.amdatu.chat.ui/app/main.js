/**
 * Created by paul on 05/09/14.
 */
require
    .config({
        paths: {
            'angular': 'bower_components/angular/angular',
            'angular-route': 'bower_components/angular-route/angular-route',
            'domReady': 'bower_components/requirejs-domready/domReady',
            'SockJS': 'bower_components/sockjs/sockjs',
            'Rx' : 'bower_components/rxjs/dist/rx.lite',
            'Stomp' : 'bower_components/stomp-websocket/lib/stomp'
        },

        shim: {
            'angular': {
                exports: 'angular'
            },

            'angular-route': {
                deps: ['angular']
            },
            'SockJS' : {
                exports: 'SockJS'
            },
            'Stomp' : {
                exports: 'Stomp'
            }
        }
    }
);

require(['bootstrap', 'domReady'], function() {
    // nothing to do here...see app.bootstrap.js
});