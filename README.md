# README #

This is an Amdatu example project. The project represents a chat application with a backend that stores messages and recommends products to users based on popular topics in a chat room.
The application consists of several components to show how a larger application would look like. Note that the same model scales to much larger code bases in the exact same way.

![chat.jpg](https://bitbucket.org/repo/eoMg6K/images/3744310611-chat.jpg)

### Technology included ###

In the backend: 

* Amdatu REST
* Amdatu Web
* Amdatu Mongo
* Amdatu JPA
* Amdatu Scheduling
* Amdatu Configurator
* Amdatu Testing
* RabbitMQ

On the client side: 

* AngularJS
* SockJS
* RequireJS
* RxJS

### How do I get set up? ###

* Install and run MongoDB
* Install and run RabbitMQ
* Enable [Stomp Web module](http://www.rabbitmq.com/web-stomp.html) on RabbitMQ
* Setup an exchange in RabbitMQ as shown on the screenshot below
* Clone the repository
* Import all projects in Bndtools
* Run the UI build using Grunt (see below for instructions)
* Run run/demo.bndrun

![rabbitconfig.png](https://bitbucket.org/repo/eoMg6K/images/2293987627-rabbitconfig.png)

### Running the UI build ###

The UI is build using Grunt.

* cd org.amdatu.chat.ui
* npm install
* grunt

### Mongo test data ###

To see the product recommendations in action there should be a Mongo database 'amdatu-chat' with the following contents:

```
db.products.save({name: 'Building Modular Cloud apps with OSGi', imageUrl: 'http://akamaicovers.oreilly.com/images/0636920028086/cat.gif', tags: ['OSGi', 'modularity']})

```