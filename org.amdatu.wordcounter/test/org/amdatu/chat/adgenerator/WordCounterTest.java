package org.amdatu.chat.adgenerator;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.amdatu.chat.wordcounter.impl.LamdaWordCounter;
import org.junit.Test;

public class WordCounterTest {

	@Test
	public void test() {
		List<String> lines = Arrays.asList("this is about Amdatu", "Amdatu is pretty cool", "what's this Amdatu stuff?");
		
		LamdaWordCounter counter = new LamdaWordCounter();
		assertEquals("Amdatu", counter.getMostPopular(lines));
	}

}
