package org.amdatu.chat.chatlog.jpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.amdatu.chat.chatlog.Message;

@Entity
public class JPAMessage {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String room;
	private String fromUser;
	private String body;
	private long ts;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getFrom() {
		return fromUser;
	}

	public void setFrom(String from) {
		this.fromUser = from;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public long getTs() {
		return ts;
	}

	public void setTs(long timestamp) {
		this.ts = timestamp;
	}

	public Message toMessage() {
		Message m = new Message();
		m.setRoom(room);
		m.setBody(body);
		m.setFrom(fromUser);
		
		return m;
	}
}
