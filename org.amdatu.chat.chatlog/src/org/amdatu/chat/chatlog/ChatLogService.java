package org.amdatu.chat.chatlog;

import java.util.List;

public interface ChatLogService {
	void save(Message message);
	List<Message> getLastMessages(String room, int nrOfMessages);
}
