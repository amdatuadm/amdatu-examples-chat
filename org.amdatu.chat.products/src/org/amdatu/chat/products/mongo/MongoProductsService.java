package org.amdatu.chat.products.mongo;

import java.util.ArrayList;
import java.util.List;

import org.amdatu.chat.products.Product;
import org.amdatu.chat.products.ProductService;
import org.amdatu.mongo.MongoDBService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

@Component
public class MongoProductsService implements ProductService{
	@ServiceDependency
	private volatile MongoDBService m_mongoDbService;
	
	private volatile Jongo m_jongo;
	private volatile MongoCollection m_products;
	
	@Start
	public void start() {
		m_jongo = new Jongo(m_mongoDbService.getDB());
		m_products = m_jongo.getCollection("products");
	}
	
	
	
	@Override
	public List<Product> listProductsByTag(String tag) {
		
		List<Product> result = new ArrayList<>();
		m_products.find("{tags: #}", tag)
			.as(Product.class)
			.forEach(result::add);
	
		
		return result;
	}

}
