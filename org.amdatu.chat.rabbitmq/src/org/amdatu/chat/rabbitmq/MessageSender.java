package org.amdatu.chat.rabbitmq;

import org.amdatu.chat.chatlog.Message;

public interface MessageSender {
	void send(Message message);
}
