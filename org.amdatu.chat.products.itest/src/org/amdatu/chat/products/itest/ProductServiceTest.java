package org.amdatu.chat.products.itest;

import static org.amdatu.testing.configurator.OSGiTestConfigurator.configureFactory;
import static org.amdatu.testing.configurator.OSGiTestConfigurator.configureTest;
import static org.amdatu.testing.configurator.OSGiTestConfigurator.inject;

import java.util.List;

import org.amdatu.chat.products.Product;
import org.amdatu.chat.products.ProductService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProductServiceTest {
	
	private volatile ProductService m_productService;
	
	@Before
	public void setUp() throws Exception {
		
		configureTest(this, 
				configureFactory("org.amdatu.mongo").set("dbName", "amdatu-chat"), 
				inject(ProductService.class));
	}
	
	@Test
	public void listByTag() {
		List<Product> products = m_productService.listProductsByTag("OSGi");
		Assert.assertEquals(1, products.size());
	}
}
